/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package khartn.khartnosgiwrapper;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.launch.Framework;
import org.osgi.framework.launch.FrameworkFactory;

public class OSGiLoader {

    public static Framework m_framework = null;

    /**
     *
     * @param args HashMap для конфигурирования KhArtNOSGiWrapper.
     *
     * <br> Ключи: <br> <b>showDefaultMessageOnStart</b> - показывать при старте
     * сообщение по умолчанию. Если не указано или равно true, то выводится
     * сообщение по умолчанию.
     *
     * <br> <b>customMessageOnStart </b>- текст сообщения, которое будет
     * показываться при старте (как альтернатива стандартному сообщению).
     *
     * <br> <b>showCustomMessageOnStart</b> - показывать текст сообщения из
     * customMessageOnStart при запуске. Если равно true и существует запись с
     * ключём customMessageOnStart, то при старте выводится сообщение, взятое из
     * записи с ключём customMessageOnStart
     *
     * <br> <b>pathToFelixCache</b> - путь, по которому будет распологаться кэш
     * Felix. Если не указана запись с таким ключём, то путь для кэша Felix
     * будет принят как "target/cache"
     *
     * <br> <b>configFelix</b> - Запись, содержащая объект HashMap. Записи
     * данного объекта присваиваются к внутренней HashMap, которая содержит
     * настройки для создания объекта Felix, а именно для      <code>
     * m_framework = createFramework(config);
     * </code> Присвоение к внутренней HashMap производится после её
     * инициализации, т.е. после      <code>
     * Map<String, String> config = ConfigUtil.createConfig(pathToFelixCache);
     * </code> что даёт возможность перезаписать необходимые ключи.
     */
    public void start(HashMap<String, Object> args) {

        removeDir(args.get("pathToFelixCache").toString());

        // Args should never be null if the application is run from the command line.
        // Check it anyway.
        ArrayList<String> locations = new ArrayList();

        indexBundlesDir(args.get("InitialBundlesDir").toString(), locations);

        // Print welcome banner.
        if (args.get("showDefaultMessageOnStart") == null || (Boolean) args.get("showDefaultMessageOnStart")) {
            System.out.println("\nWelcome to KhArtNOSGiWrapper");
            System.out.println("======================\n");
        }

        if (args.get("showCustomMessageOnStart") != null && args.get("customMessageOnStart") != null && (Boolean) args.get("showCustomMessageOnStart")) {
            System.out.println(args.get("customMessageOnStart"));
            System.out.println("======================\n");
        }

        try {
            String pathToFelixCache = "";
            if (args.get("pathToFelixCache") == null) {
                pathToFelixCache = "target/cache";
            } else {
                pathToFelixCache = (String) args.get("pathToFelixCache");
            }

            Map<String, String> config = ConfigUtil.createConfig(pathToFelixCache);

            if (args.get("configFelix") != null) {
                config.putAll((HashMap) args.get("configFelix"));
            }

            config.put("org.osgi.framework.system.packages.extra", "khartn.khartnosgiwrapper");


            m_framework = createFramework(config);
            m_framework.init();
            m_framework.start();
            installAndStartBundles(locations);

//            int x = 0;
//            for (Bundle qqq : m_framework.getBundleContext().getBundles()) {
//                if (x < 1) {
//                    HashSet<Bundle> bundles;
//                    bundles = new HashSet<Bundle>();
//                    bundles.add(qqq);
//                    HashSet<Bundle> depends = (HashSet<Bundle>) m_framework.adapt(FrameworkWiring.class).getDependencyClosure(bundles);
//                    System.out.println("!!!---");
//                    System.out.println(qqq.getSymbolicName());
//                    System.out.println("!!!+++");
//                    for (Bundle depends1 : depends) {
//                        System.out.println(depends1.getSymbolicName());
//                    }
//                }
////                x++;
//            }



            m_framework.waitForStop(0);
            System.exit(0);
        } catch (Exception ex) {
            System.err.println("Could not create framework: " + ex);
            ex.printStackTrace();
            System.exit(-1);
        }
    }

    private void removeDir(String pathToDir) {
        File dir = new File(pathToDir);
        if (dir.exists()) {
            for (File file : dir.listFiles()) {
                if (file.isDirectory()) {
                    removeDir(file.getAbsolutePath());
                }
                file.delete();
            }
            dir.delete();
        }
    }

    private static void indexBundlesDir(String bundlesDir, ArrayList<String> locations) {
        File dir = new File(bundlesDir);
        String[] children = dir.list();
        if (children == null) {
            // Either dir does not exist or is not a directory
        } else {
            for (int i = 0; i < children.length; i++) {
                // Get filename of file or directory
                File tmpFile = new File("");
                locations.add("file:/" + tmpFile.getAbsolutePath() + "/" + bundlesDir + "/" + children[i]);
            }
        }
    }

    /**
     * Util method for creating an embedded Framework. Tries to create a
     * {@link FrameworkFactory} which is then be used to create the framework.
     *
     * @param config the configuration to create the framework with
     * @return a Framework with the given configuration
     */
    private static Framework createFramework(Map<String, String> config) {
        ServiceLoader<FrameworkFactory> factoryLoader = ServiceLoader.load(FrameworkFactory.class);
        for (FrameworkFactory factory : factoryLoader) {
            return factory.newFramework(config);
        }
        throw new IllegalStateException("Unable to load FrameworkFactory service.");
    }

    /**
     * Installs and starts all bundles used by the application. Therefore the
     * host bundle will be started. The locations of extensions for the host
     * bundle can be passed in as parameters.
     *
     * @param bundleLocations the locations where extension for the host bundle
     * are located. Must not be {@code null}!
     * @throws BundleException if something went wrong while installing or
     * starting the bundles.
     */
    private static void installAndStartBundles(ArrayList<String> bundleLocations) throws BundleException {
        BundleContext bundleContext = m_framework.getBundleContext();
        for (String location : bundleLocations) {
            Bundle addition = bundleContext.installBundle(location);
            addition.start();
        }
    }
}
