package khartn.i_mainloader;

import java.io.File;
import java.util.ArrayList;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;

public class Activator implements BundleActivator {

    @Override
    public void start(BundleContext context) throws Exception {
        // TODO add activation code here
        
        // Args should never be null if the application is run from the command line.
        // Check it anyway.
        ArrayList<String> locations = new ArrayList();
//        ArrayList<String> locations = new ArrayList<>();

        indexBundlesDir("I_Bundles/Stage_300", locations);
        indexBundlesDir("I_Bundles/Stage_300a", locations);
        indexBundlesDir("I_Bundles/Stage_400", locations);
        indexBundlesDir("I_Bundles/Stage_500", locations);

        try {
            installAndStartBundles(locations, context);

//            for (Bundle testBundle : m_framework.getBundleContext().getBundles()) {
//                if (testBundle.getSymbolicName().equals("ihtika2.I_TestBundle")) {
//                    System.out.println("found");
//
//
//                    ServiceReference[] refs;
//                    try {
//                        BundleContext bundleContext = testBundle.getBundleContext();
//
////            System.out.println(TestClasssInter.class.getName());
//                        refs = bundleContext.getServiceReferences(TestClasssInter.class.getName(), "(Funct=TESTCl)");
//
//                        if (refs == null) {
//                            System.out.println("Not Found AboutForm on show!!!");
//                        } else {
//                            Object MainForm = bundleContext.getService(refs[0]);
//                            TestClasssInter sdfsdf = (TestClasssInter) MainForm;
//                            System.out.println("Printing2!!!");
//                            sdfsdf.printSomeLine();
////                    MainForm.sendContext(bundleContext);
////                    MainForm.showWindow();
//                        }
//
//                    } catch (InvalidSyntaxException ex) {
//                        ex.printStackTrace();
//                    }
//
//                }
////                Dictionary<String, String> headerLine = testBundle.getHeaders();
////                Enumeration e = headerLine.keys();
////
////                while (e.hasMoreElements()) {
////                    Object key = e.nextElement();
////                    if (key.equals("Import-Package")) {
////                        System.out.println(key + " - " + headerLine.get(key));
////                    }
////                    System.out.println(key + " - " + headerLine.get(key));
////                }
//            }

        } catch (Exception ex) {
            System.err.println("Could not create framework: " + ex);
            ex.printStackTrace();
            System.exit(-1);
        }
        
    }
    
    /**
     * Installs and starts all bundles used by the application. Therefore the
     * host bundle will be started. The locations of extensions for the host
     * bundle can be passed in as parameters.
     *
     * @param bundleLocations the locations where extension for the host bundle
     * are located. Must not be {@code null}!
     * @throws BundleException if something went wrong while installing or
     * starting the bundles.
     */
    private static void installAndStartBundles(ArrayList<String> bundleLocations, BundleContext context) throws BundleException {
        BundleContext bundleContext = context;
//        Activator bundleActivator = new Activator();
//        bundleActivator.start(bundleContext);
        for (String location : bundleLocations) {
            Bundle addition = bundleContext.installBundle(location);
//            System.out.println(location);
            addition.start();
        }
    }
    
    private static void indexBundlesDir(String bundlesDir, ArrayList<String> locations) {
        File dir = new File(bundlesDir);
        String[] children = dir.list();
        if (children == null) {
            // Either dir does not exist or is not a directory
        } else {
            for (int i = 0; i < children.length; i++) {
                // Get filename of file or directory
                File tmpFile = new File("");
                locations.add("file:/" + tmpFile.getAbsolutePath() + "/" + bundlesDir + "/" + children[i]);
            }
        }
    }
    

    @Override
    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }

}
